﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using TuringMachineLibrary;

namespace TuringMachineSimulator
{
    internal class Application
    {
        private static string LoadResource(string fileName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = assembly.GetManifestResourceNames().Single(str => str.EndsWith(fileName));
            using (var stream = assembly.GetManifestResourceStream(resourceName))
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        private static void Main(string[] args)
        {
/*            var rules = new List<Rule> {new Rule(new Input("r0", 0), new Output("r1", 1, Motion.Right))};

            var program = new Program();
            program.FromRules(rules);*/


            /*var serializedProgram = JsonConvert.SerializeObject(program);
            Console.WriteLine(serializedProgram);


            var program2 = JsonConvert.DeserializeObject<Program>(serializedProgram);*/


            /*var program =
                Program.Deserialize(
                    File.ReadAllText("/Users/lmureu/Library/Preferences/Rider2019.1/scratches/scratch.json"));
*/

            /*var turing = new TuringMachine(program, "r0", new[]{"t"});
            
            Console.WriteLine(JsonConvert.SerializeObject(turing));

            return;*/


            var turing = TuringMachine.Deserialize(LoadResource("turing.json"));


            do
            {
                turing.Step();
                Console.WriteLine(turing);
            } while (turing.IsRunning);

            Console.WriteLine(turing.Serialize());
        }
    }
}