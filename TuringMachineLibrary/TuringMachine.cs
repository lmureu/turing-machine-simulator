using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace TuringMachineLibrary
{
    public class TuringMachine
    {
        [JsonConstructor]
        public TuringMachine(Tape tape, Program program, string state, string[] terminationStates)
        {
            Tape = tape;
            Program = program;
            State = state;
            TerminationStates = terminationStates;
            IsRunning = true;
        }

        public TuringMachine(Program program, string state, string[] terminationStates)
            : this(new Tape(), program, state, terminationStates)
        {
        }

        public Tape Tape { get; }
        public Program Program { get; }
        public string State { get; private set; }
        public string[] TerminationStates { get; }

        public bool IsRunning { get; private set; }

        [JsonIgnore] public bool Accepted => !IsRunning && HasTerminated;

        private bool HasTerminated => TerminationStates.Contains(State);

        public void Step()
        {
            if (!IsRunning) return;

            if (HasTerminated)
            {
                IsRunning = false;
                return;
            }

            var input = new Input(State, Tape.Read());
            ;
            Output output;

            try
            {
                output = Program[input];
            }
            catch (KeyNotFoundException)
            {
                IsRunning = false;
                return;
            }


            State = output.State;

            Tape.Write(output.Value);

            switch (output.Motion)
            {
                case Motion.Left:
                    Tape.MoveLeft();
                    break;

                case Motion.Right:
                    Tape.MoveRight();
                    break;
            }
        }

        public override string ToString()
        {
            return $"State: {State}; Tape: {Tape}; IsRunning: {IsRunning}; Accepted: {Accepted}";
        }

        public string Serialize()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static TuringMachine Deserialize(string json)
        {
            return JsonConvert.DeserializeObject<TuringMachine>(json);
        }
    }
}