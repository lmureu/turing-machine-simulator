using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace TuringMachineLibrary
{
    public class Tape
    {
        public Tape(byte defaultValue) : this(new Dictionary<int, byte>(), 0, defaultValue)
        {
        }

        [JsonConstructor]
        public Tape(Dictionary<int, byte> values, int index, byte defaultValue)
        {
            Values = values;
            Index = index;
            DefaultValue = defaultValue;
        }

        public Tape() : this(0)
        {
        }

        public Dictionary<int, byte> Values { get; }
        public int Index { get; private set; }
        public byte DefaultValue { get; }

        public void MoveRight()
        {
            Index++;
        }

        public void MoveLeft()
        {
            Index--;
        }

        public byte Read()
        {
            return Get(Index);
        }

        public void Write(byte value)
        {
            Values[Index] = value;
        }

        private byte Get(int index)
        {
            return Values.GetValueOrDefault(index, DefaultValue);
        }


        public override string ToString()
        {
            var max = Math.Max(Index, Values.Keys.Max());
            var min = Math.Min(Index, Values.Keys.Min());

            var list = new List<string>();
            for (var i = min; i <= max; i++)
            {
                var value = Get(i);

                var rep = i == Index ? $"[{value}]" : value.ToString();

                if (i == 0) rep = $">{rep}<";


                list.Add(rep);
            }

            return string.Join(", ", list);
        }
    }
}