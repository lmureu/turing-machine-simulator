using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace TuringMachineLibrary
{
    public class Program
    {
        private readonly Dictionary<Input, Output> _rulesDictionary = new Dictionary<Input, Output>();

        public IEnumerable<Rule> Rules
        {
            get => ToRules();
            set => FromRules(value);
        }

        public Output this[Input input]
        {
            get => _rulesDictionary[input];
            set => _rulesDictionary[input] = value;
        }

        public void FromRules(IEnumerable<Rule> rules)
        {
            foreach (var rule in rules) _rulesDictionary[rule.Input] = rule.Output;
        }

        public IEnumerable<Rule> ToRules()
        {
            return _rulesDictionary.Select(pair => new Rule(pair.Key, pair.Value)).ToImmutableArray();
        }
    }


    public struct Rule
    {
        public Input Input { get; }
        public Output Output { get; }

        public Rule(Input input, Output output)
        {
            Input = input;
            Output = output;
        }
    }

    public struct Input
    {
        public string State { get; }
        public byte Value { get; }

        public Input(string state, byte value)
        {
            State = state;
            Value = value;
        }
    }

    public struct Output
    {
        public string State { get; }
        public byte Value { get; }
        public Motion Motion { get; }

        public Output(string state, byte value, Motion motion)
        {
            State = state;
            Value = value;
            Motion = motion;
        }
    }

    public enum Motion
    {
        None,
        Left,
        Right
    }
}